package com.company;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BasketTest {
    private ShopOperation shop;

    @BeforeEach
    public void init() {
        shop = new Shop();
    }

    @Test
    public void checkCorrectOperationsForBasket() {
        assertEquals(1, shop.addProductToBasket("chleb", 100), "dodanie produktu nr: 1");
        assertEquals(2, shop.addProductToBasket("herbata", 200), "dodanie produktu nr: 2");
        assertEquals(3, shop.addProductToBasket("cukier", 300), "dodanie produktu nr: 3");
        assertEquals(4, shop.addProductToBasket("woda", 400), "dodanie produktu nr: 4");
        assertEquals(5, shop.addProductToBasket("kawa", 500), "dodanie produktu nr: 5");
        assertEquals(6, shop.addProductToBasket("nutella", 600), "dodanie produktu nr: 6");
        assertEquals(1, shop.removeProductFromBasket(1), "usunięto produkt nr: 1");
        assertEquals(3, shop.removeProductFromBasket(3), "usunięto produkt nr: 3");
        assertEquals(-1, shop.removeProductFromBasket(7), "usunięto produkt nr: 7");
        assertEquals(5, shop.addProductToBasket("marchewka", 150), "dodanie produktu nr: 5");
        assertEquals(1750, shop.getTotalPriceInBasket(), "stan koszyka wynosi: 1750");
        assertEquals(4, shop.removeProductFromBasket(4), "usunięto produkt nr: 4");
        assertEquals(4, shop.quantityProducts(), "liczba produktów w koszyku: 4");
        assertEquals(200, shop.getPriceProduct(1), "koszt produktu nr: 1");
        assertEquals(500, shop.getPriceProduct(3), "koszt produktu nr: 3");
        assertEquals(1150, shop.getTotalPriceInBasket(), "stan koszyka: 1150");
    }
}
