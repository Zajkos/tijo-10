package com.company;

public interface ShopOperation {
    int PRODUCT_NOT_EXISTS = -1;
    int INCORRECT_PRICE = -1;

    int addProductToBasket(String nameProduct, int price);

    int removeProductFromBasket(int numberProduct);

    int quantityProducts();

    int getPriceProduct(int numberProduct);

    int getTotalPriceInBasket();
}
