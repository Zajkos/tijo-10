package com.company;

public class Basket {
    private String nameProduct;
    private int numberProduct;
    private int price;

    public Basket(String nameProduct, int numberProduct, int price) {
        this.nameProduct = nameProduct;
        this.numberProduct = numberProduct;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
