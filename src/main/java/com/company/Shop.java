package com.company;

import java.util.ArrayList;

public class Shop implements ShopOperation {
    private ArrayList<Basket> listOfProducts = new ArrayList<>();
    private int amount = 0;

    public int addProductToBasket(String nameProduct, int price) {
        Basket basket = new Basket(nameProduct, amount, price);
        if (price < 0) {
            return INCORRECT_PRICE;
        } else {
            listOfProducts.add(basket);
            amount = listOfProducts.size();
            return amount;
        }
    }

    public int removeProductFromBasket(int numberProduct) {
        if (numberProduct <= amount && numberProduct > 0) {
            listOfProducts.remove(listOfProducts.get(numberProduct - 1));
            return numberProduct;
        }
        return PRODUCT_NOT_EXISTS;
    }

    public int quantityProducts() {
        if (listOfProducts.size() == 0) {
            return 0;
        } else {
            return listOfProducts.size();
        }
    }

    public int getPriceProduct(int numberProduct) {
        return listOfProducts.get(numberProduct - 1).getPrice();
    }

    public int getTotalPriceInBasket() {
        int totalPrice = 0;
        for (Basket b : listOfProducts) {
            totalPrice += b.getPrice();
        }
        return totalPrice;
    }
}
